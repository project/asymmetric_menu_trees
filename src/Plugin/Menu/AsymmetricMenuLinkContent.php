<?php

namespace Drupal\asymmetric_menu_trees\Plugin\Menu;

use Drupal\menu_link_content\Plugin\Menu\MenuLinkContent;

/**
 * Provides the menu link plugin for content menu links.
 */
class AsymmetricMenuLinkContent extends MenuLinkContent {

  /**
   * {@inheritdoc}
   */
  public function isEnabled() {
    // Get the value from entity.
    if ($this->languageManager->isMultilingual()) {
      return $this->getEntity()->isEnabled();
    }
    // Fallback to plugin definition.
    return (bool) $this->pluginDefinition['enabled'];
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight() {
    // Get the value from entity.
    if ($this->languageManager->isMultilingual()) {
      return $this->getEntity()->getWeight();
    }

    // Or set default the weight to 0.
    if (!isset($this->pluginDefinition['weight'])) {
      $this->pluginDefinition['weight'] = 0;
    }
    // Fallback to plugin definition.
    return $this->pluginDefinition['weight'];
  }

  /**
   * {@inheritdoc}
   */
  public function getUrlObject($title_attribute = TRUE) {
    // Get the value from entity.
    if ($this->languageManager->isMultilingual()) {
      return $this->getEntity()->getUrlObject();
    }
    // Or send the default link.
    return parent::getUrlObject($title_attribute);
  }

  /**
   * {@inheritdoc}
   */
  public function getParent() {
    // Get the value from entity.
    if ($this->languageManager->isMultilingual()) {
      return $this->getEntity()->getParentId();
    }

    return $this->pluginDefinition['parent'];
  }

  /**
   * Get the link entity associated with this menu link Plugin.
   */
  public function getMenuLinkEntity() {
    return $this->getEntity();
  }

}
