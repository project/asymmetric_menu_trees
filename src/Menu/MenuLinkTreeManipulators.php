<?php

namespace Drupal\asymmetric_menu_trees\Menu;

use Drupal\Core\Menu\MenuLinkBase;
use Drupal\asymmetric_menu_trees\Plugin\Menu\AsymmetricMenuLinkContent;
use Drupal\Core\Language\LanguageManagerInterface;

/**
 * Provides menu link tree manipulators.
 */
class MenuLinkTreeManipulators {
  /**
   * The plugin manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Constructs a new MenuLinkTreeManipulator.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The plugin manager.
   */
  public function __construct(LanguageManagerInterface $language_manager) {
    $this->languageManager = $language_manager;
  }

  /**
   * Remove disabled links from the menu tree.
   *
   * @param array $tree
   *   Something.
   */
  public function removeDisabledLinks(array $tree) {
    foreach ($tree as $key => $element) {
      if ($tree[$key]->subtree) {
        $tree[$key]->subtree = $this->removeDisabledLinks($tree[$key]->subtree);
      }
      if ($element->link instanceof MenuLinkBase) {
        $link = $element->link;
        if (!$link->isEnabled()) {
          unset($tree[$key]);
        }
      }
    }

    return $tree;
  }

  /**
   * Restructure the menu tree to make it asymmetric.
   *
   * Menu tree storage in Drupal is implemented in a way that it saves only one
   * structure for the tree for all languages. In fact, it is langauge
   * independent. This makes it impossible to have different tree structure
   * for different languages.
   * The main caveat in core's way of doing this is that even if we some how
   * are able to have different 'parent' for a menu link in different
   * languages, in the tree the menu link has only one parent which is used for
   * all langauge, because, as we previously stated, the menu tree in core is
   * langauge independent.
   * As part of this manipulator, we traverse through the tree and restructure
   * it for current langauge based on parent-child relationship of menu links
   * for current language.
   *
   * @param array $tree
   *   Menu Tree.
   */
  public function restructureTree(array $tree) {
    $rootTree = new \stdClass();
    $rootTree->subtree = $tree;

    $stack = [];
    $allTrees = [];

    array_push($stack, $rootTree);

    $currentTree = $rootTree;
    $current = key($currentTree->subtree);

    // Iteratively traverse the tree to get all the tree objects in an array
    // for structure manipulation.
    while ($current !== NULL) {
      $current = key($currentTree->subtree);
      while ($current !== NULL) {
        array_push($stack, $currentTree);
        $allTrees[$current] = $currentTree->subtree[$current];
        $currentTree = $currentTree->subtree[$current];
        $current = key($currentTree->subtree);
      }
      while ($current === NULL && !empty($stack)) {
        $currentTree = array_pop($stack);
        next($currentTree->subtree);
        $current = key($currentTree->subtree);
      }
    }

    if ($current === NULL) {
      array_pop($stack);
    }

    // Restructure all subtrees of the main menu tree.
    foreach ($allTrees as $currentTree) {
      foreach ($currentTree->subtree as $key => $subtree) {
        if (!empty($subtree->link) && $subtree->link instanceof AsymmetricMenuLinkContent) {
          // Get Parent Tree information.
          $parentTree = $subtree->link->getParent();
          // If this subtree has no parent, make this a top level subtree &
          // set depth to 1.
          if (empty($parentTree)) {
            unset($currentTree->subtree[$key]);
            $tree[$key] = $subtree;
            $subtree->depth = 1;
          }
          else {
            // Update depth of current tree and hasChildren properties
            // of its parent.
            if (!empty($allTrees[$parentTree])) {
              $allTrees[$parentTree]->hasChildren = TRUE;
              $subtree->depth = $allTrees[$parentTree]->depth + 1;
            }
            // Assign as subtree to new parent.
            unset($currentTree->subtree[$key]);
            $allTrees[$parentTree]->subtree[$key] = $subtree;
          }
        }
      }
    }

    // Restructure all top elements of the main menu tree.
    foreach ($allTrees as $currentKey => $currentTree) {
      // Get Parent Tree information.
      if (!empty($currentTree->link) && $currentTree->link instanceof AsymmetricMenuLinkContent) {
        $currentParentTreeId = $currentTree->link->getParent();
        // If this subtree has no parent, make this a top level subtree.
        if (empty($currentParentTreeId)) {
          unset($tree[$currentKey]);
          $tree[$currentKey] = $currentTree;
        }
        else {
          // Update depth of current tree and hasChildren properties
          // of its parent.
          if (!empty($allTrees[$currentParentTreeId])) {
            $allTrees[$currentParentTreeId]->hasChildren = TRUE;
            $currentTree->depth = $allTrees[$currentParentTreeId]->depth + 1;
          }
          // Assign as subtree to new parent.
          unset($tree[$currentKey]);
          $allTrees[$currentParentTreeId]->subtree[$currentKey] = $currentTree;
        }
      }
    }

    // The iteration above might have removed child trees for some trees.
    // Set hasChildren properties of such trees to FALSE.
    // Also, set hasChildren to FALSE for those elements who has no childeren
    // of same language.
    foreach ($allTrees as $currentTree) {
      // If no links in subtree, set hasChildren to FALSE.
      if (empty($currentTree->subtree)) {
        $currentTree->hasChildren = FALSE;
      }
      // Iterate through all links in subtree. If any one of those are of same
      // or fallback language, keep hasChildren to TRUE.
      // If all of them are false, set hasChildren to FALSE.
      else {
        foreach ($currentTree->subtree as $subtree) {
          if (!empty($subtree->link) && $subtree->link instanceof AsymmetricMenuLinkContent) {
            $menuLinkLanguage = $subtree->link->getMenuLinkEntity()->language()->getId();
            $fallbackLanguages = $this->languageManager->getFallbackCandidates([
              'langcode' => $this->languageManager->getCurrentLanguage()->getId(),
            ]);

            if (in_array($menuLinkLanguage, $fallbackLanguages)) {
              $currentTree->hasChildren = TRUE;
              break;
            }
            else {
              $currentTree->hasChildren = FALSE;
            }
          }
        }
      }
    }
    return $tree;
  }

}
