<?php

namespace Drupal\asymmetric_menu_trees\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class AsymmetricMenuTreesConfigForm.
 */
class AsymmetricMenuTreesConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'asymmetric_menu_trees.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'asymmetric_menu_trees_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('asymmetric_menu_trees.settings');
    $form['multilingual'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Choose the features to enable.'),
      '#options' => [
        'link' => $this->t('Different url for a link for different languages'),
        'order' => $this->t('Different ordering of links in menu tree for different languages'),
        'enabled' => $this->t('Menu links enabled for some languages and disabled for some other languages'),
      ],
      '#default_value' => $config->get('multilingual'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('asymmetric_menu_trees.settings')
      ->set('multilingual', $form_state->getValue('multilingual'))
      ->save();

    drupal_flush_all_caches();
  }

}
