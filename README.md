CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The menu system in Drupal core provides us the ability to translate
menus for different languages in two ways:

1. Create a single menu for all languages and translate the terms.
2. Create different menus for different languages.

This module deals with the shortcomings of the first way.

One major shortcoming of the first way is that the structure of
menu trees is fixed i.e. one can't change the following properties 
of the menu links in a tree for different languages:

1. Weight
2. Enabled
3. Link URL

This means that the menu tree remains 'symmetric' in all languages.

This module makes the menu trees asymmetric by the ability to change
the following properties of the menu links in a tree for different
languages:

1. Weight
2. Enabled
3. Link URL

 * For a full description of the module visit:
   https://www.drupal.org/project/asymmetric_menu_trees

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/asymmetric_menu_trees


REQUIREMENTS
------------

 * This module requires no modules outside of Drupal core.


INSTALLATION
------------

Install the Asymmetric menu trees module as you would normally install a
contributed Drupal module. Visit https://www.drupal.org/node/1897420 for 
further information.


CONFIGURATION
-------------

1. Navigate to `Administration > Extend` and enable the module.
2. Navigate to `Administration > Configuration > Asymmetric Menu Trees'.


MAINTAINERS
-----------

 * Dipak Yadav (dipakmdhrm) - https://www.drupal.org/u/dipakmdhrm
